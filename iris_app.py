import os
import joblib
import numpy as np
import pandas as pd
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier

from flask import Flask, request, flash, jsonify, abort, redirect, url_for, render_template, send_file, after_this_request
from flask_wtf import FlaskForm
from markupsafe import escape
from wtforms import StringField, FileField
from wtforms.validators import DataRequired
from werkzeug.utils import secure_filename

app = Flask(__name__)

# Using a development configuration
app.config.from_object('config.DevConfig')

#model
knn = joblib.load('knn.pkl')

@app.route('/')
def index():
    return redirect(url_for('iris_form'))

@app.route('/badrequest')
def bad_request():
    return abort(400)

@app.route('/iris_post', methods = ['POST'])
def iris_post():
    """Return answer for 1 flower"""
    try:
        content = request.get_json()

        params = content['flower'].split(',')
        params = [float(num) for num in params]

        params = np.array(params).reshape(1,-1)
        predict = {'class':str(knn.predict(params)[0])}
    except:        
        return redirect(url_for('bad_request'))

    return jsonify(predict)

#form for user on FlaskForm

class MyForm(FlaskForm):
    file = FileField()

@app.route('/iris_form', methods=('GET', 'POST'))
def iris_form():
    """Predict with form"""
    form = MyForm()
    if form.validate_on_submit():

        uploaded_file = form.file.data

        [file_handle, filename] = read_predict_delete(uploaded_file)

        return send_file(file_handle, mimetype='text/csv', attachment_filename=filename, as_attachment=True)
    return render_template('iris_form.html', form=form)

#form for service and user

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in {'csv'}

@app.route('/api/predict', methods=['GET', 'POST'])
def upload_file():
    """Predict with post"""
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            uploaded_file = file

            [file_handle, filename] = read_predict_delete(uploaded_file)

            return send_file(file_handle, mimetype='text/csv', attachment_filename=filename, as_attachment=True)
    return redirect(url_for('iris_form'))

def read_predict_delete(uploaded_file):
    """All important action for predict.

    Keyword arguments:
    uploaded_file -- the real part (default 0.0)

    """
    filename = secure_filename(uploaded_file.filename) + '_answers.csv'
    file_path = app.config['UPLOAD_FOLDER'] + '/' + filename

    #if need save
    #file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

    df = pd.read_csv(uploaded_file, header=None)
    predict = knn.predict(df)
    result = pd.DataFrame(predict)
    result.to_csv(file_path, index=False, header=None)

    #remove file after upload
    file_handle = open(file_path, 'r')

    @after_this_request 
    def remove_file(response): 
        os.remove(file_path) 
        return response

    return [file_handle, filename]