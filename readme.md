# Run docker

### in
docker exec -it flusk-hello_flask_1 bash

### train
docker exec -it flusk-hello_flask_1 python train_model.py

### build and run
docker-compose up --build

# Request

### post one flower
curl --header "Content-Type: application/json" --request POST --data '{"flower":"1,2,3,4"}' http://localhost:5000/iris_post

### upload file in interface
http://localhost:5000/iris_form - send file with 4 feature

### upload file by request
http://localhost:5000/api/predict - send file with 4 feature